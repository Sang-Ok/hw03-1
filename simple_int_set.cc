#ifndef _SIMPLE_INT_SET_CC_
#define _SIMPLE_INT_SET_CC_

// simple_int_set.cc
#include "simple_int_set.h"

using namespace std;
#define  TEST(str1,value) cout<<"\t\tTEST_STATEMENT::"<<str1<<value<<endl;

SimpleIntSet::SimpleIntSet(){
	alloc_	=0;
	size_	=0;
	values_	=NULL;
}

SimpleIntSet::SimpleIntSet(const SimpleIntSet& int_set){
	alloc_	=int_set.alloc_;
	size_	=int_set.size_;
	values_	=new int[alloc_];
	for(int i; i<size_; i++){
		values_[i]=int_set.values_[i];
	}
}
SimpleIntSet::~SimpleIntSet(){
	delete[] values_;
}

void SimpleIntSet::Add(const int value){
	if(size_<alloc_){
		//TEST("not resize for:",value);
	}
	else
	{
		//TEST("resizing for  :",value);
		//TEST("old size was  :",alloc_);
		while(alloc_ < size_+1)
			alloc_	=alloc_*2;
		//TEST("new size is   :",alloc_);
		int* old_values	=values_;
		values_		=new int[alloc_];
		for(int i=0;  i<size_;  i++)
			values_[i]=old_values[i];
		delete[] old_values;
	}
	values_[size_++]=value;
	//TEST("ADDED DATA IS:",value);
}

SimpleIntSet SimpleIntSet::Intersect(const SimpleIntSet& int_set) const{
	SimpleIntSet return_set;
	int i,j;
	for(i=0; i<size_;i++)
		for(j=0;j<int_set.size_;j++)
			if(values_[i]==int_set.values_[j]){
				return_set.Add(values_[i]);
				break;
			}
	return return_set;
}

void SimpleIntSet::Sort(){
	int i, j, temp;
	for(i=0; i<size_; i++){
		for(j=i+1; j<size_ ;j++){
			if (values_[i]>values_[j]){
				temp		=values_[i];
				values_[i]	=values_[j];
				values_[j]	=temp;
			}
			else if (values_[i]==values_[j]){
				//TEST("DELETING VALUE :",values_[j]);
				//TEST("i NOW IS       :",i);
				//TEST("j NOW IS       :",j);
				values_[j]=values_[--size_];
			}
		}
	}
}

SimpleIntSet SimpleIntSet::Union(const SimpleIntSet& int_set) const{
	SimpleIntSet return_set;
	int i,j,temp;
	bool intersect;
	for(i=0; i<size_;i++)
		return_set.Add(values_[i]);
	
	for(i=0; i<int_set.size_;  i++){
		intersect=false;
		for(j=0; j<size_;  j++)
			if(int_set.values_[i]==values_[j]){
				intersect=true;
				break;
			}
		if(intersect==false)
		{
			//TEST("JUST B4 ADDING :",int_set.values_[i])
			return_set.Add(int_set.values_[i]);
		}
	}

	//SORTING
	return_set.Sort();
	return return_set;
}

SimpleIntSet SimpleIntSet::Difference(const SimpleIntSet& int_set) const{
	SimpleIntSet return_set;
	int i,j;
	bool ok;
	for(i=0; i<size_;i++){
		ok=true;
		for(j=0;j<int_set.size_;j++)
			if(values_[i]==int_set.values_[j]){				
				ok=false;
				break;
			}
		if(ok==true)
			return_set.Add(values_[i]);
	}
	return return_set;
}
void SimpleIntSet::Set(const int* values, size_t size){
	delete[] values_;
	size_=alloc_=size;
	values_	=new int[alloc_];

	int i,j,temp;
	for(i=0; i<size_; i++){
		values_[i]=values[i];
	}
	this->Sort();
}


/*
{ 11 2 3 4 5 } + { 4 5 6 7 8 }
{ 11 2 3 4 5 } * { 4 5 6 7 8 }
{ 11 2 3 4 5 } - { 4 5 6 7 8 }
*/
#endif
