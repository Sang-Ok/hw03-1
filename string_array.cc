// string_array.cc

#include "string_array.h"

using namespace std;
#define	TEST(str, val)	cout<<"\t\t"<<str<<val<<endl;

//add, set, erase, find, concat만 직접사용됨 나머진 니맘대로
StringArray::StringArray(){
	strings_= new string[4];
	alloc_=4;
	size_=0;
}

StringArray::StringArray(const StringArray& str_array){
	size_	= str_array.size_;
	alloc_	= str_array.alloc_;
	strings_= new string[alloc_];
	for(int i=0; i<size_; i++)
		strings_[i]= str_array.strings_[i];
}
StringArray::~StringArray(){
/*	int* adress= new int[4];
	delete adress;
	TEST("TEST1 :","COMPLETE");
	delete[] adress;
	TEST("TEST2 :","COMPLETE");
	TEST("DELETING, NUM OF WORDS :",size_);
*/
	delete[] strings_;
}

size_t StringArray::size() const{return size_;}

const string& StringArray:: Get(size_t i) const{	return strings_[i];}

int StringArray::Find(const std::string& str) const{		//main
	int i;
	for(i=0; i<size_; i++){
		if(strings_[i]==str)
			break;
	}
	if(i==size_) i=-1;
	return i;
}

string StringArray::Concat() const{				//main
	string ret_str;
	int i;
	for(int i=0; i<size_; i++){
		ret_str += strings_[i];
		if(i<size_-1)
			ret_str += ' ';
	}
	return ret_str;
}

void StringArray::Add(const std::string& str){			//main
	Resize(size_+1);
	strings_[size_++]=str;
}

void StringArray::Erase(size_t i){				//main
	strings_[i]="";
}

void StringArray::Set(size_t i, const std::string& str){	//main
	Resize(i);
	if(size_<i)
		size_=i+1;
	strings_[i]=str;
}

void StringArray::Copy(const StringArray& str_array){
	size_	=str_array.size_;
	alloc_	=str_array.alloc_;
	strings_=str_array.strings_;
}

void StringArray::Resize(size_t new_size){
	if(new_size<=alloc_)
		return;
	else{				//copy안써도될듯
		while(alloc_<new_size)
			alloc_ *=2;
		string* temp_str = strings_;
		strings_= new string[alloc_];
		for(int i=0;  i<size_;  i++)
			strings_[i]=temp_str[i];
		delete[] temp_str;
	}
}









































